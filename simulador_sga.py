import csv

arquivo_carros="carros.txt"
arquivo_proprietarios="proprietarios.txt"

cpf_posicao=0
nome_posicao=1

cpf_proprietario_posicao=0
chassi_posicao=1
placa_posicao=2
marca_posicao=3
modelo_posicao=4

def main():

    proprietarios=[]
    proprietarios = load_proprietarios()

    carros=[]
    carros = load_carros()

    try:

        opcao = -1
        while(opcao != 0):
            opcao = menu()
            if(opcao == 1):
                proprietarios = cadastrar_proprietario(proprietarios)
            elif(opcao == 2):
                listar_proprietarios(proprietarios)
            elif(opcao == 3):
                proprietarios = editar_proprietario(proprietarios)
            elif(opcao == 4):
                proprietarios = excluir_proprietario(proprietarios)
            elif(opcao == 5):
                carros = cadastrar_carro(carros, proprietarios)
            elif(opcao == 6):
                listar_carros(carros)
            elif(opcao == 7):
                carros = editar_carro(carros, proprietarios)
            elif(opcao == 8):
                carros = excluir_carro(carros)
            elif(opcao == 0):
                print("Finalizando...")
            else:
                print("Opção inválida")

    except KeyboardInterrupt as e:
        pass
    save_proprietarios(proprietarios)
    save_carros(carros)

def menu():
    opcao = 0
    print("\t{}\n".format("Sistema de Cadastro de Carros"))
    print("1\t{}".format("Cadastrar Proprietario"))
    print("2\t{}".format("Listar Proprietario"))
    print("3\t{}".format("Editar Proprietario"))
    print("4\t{}".format("Excluir Proprietario"))
    print("5\t{}".format("Cadastrar Carro"))
    print("6\t{}".format("Listar Carro"))
    print("7\t{}".format("Editar Carro"))
    print("8\t{}".format("Excluir Carro"))
    print("0\t{}".format("Sair"))
    opcao = int(input("\n"))
    return opcao

def cadastrar_proprietario(proprietarios):
    cpf = get_cpf()

    nome = get_nome()

    if(not any(cpf in proprietario[cpf_posicao] for proprietario in proprietarios)):
        novo_proprietario = [cpf, nome]
        proprietarios.append(novo_proprietario)
        proprietarios.sort(key=sortProprietarioByCPF)
    else:
        print("Proprietario já cadastrado")
    save_proprietarios(proprietarios)
    return proprietarios

def buscar_proprietario(proprietarios):
    posicao_proprietario = -1
    cpf = get_cpf()
    if(any(cpf in proprietario[cpf_posicao] for proprietario in proprietarios)):
        for i in range(len(proprietarios)):
            if cpf == proprietarios[i][cpf_posicao]:
                posicao_proprietario = i
                break
    else:
        pass
    return posicao_proprietario

def listar_proprietarios(proprietarios):
    opcao = -1
    while(opcao not in [0, 1, 2]):
        opcao = input("\n1-Listar todos\n2-Buscar por CPF\n0-Voltar ao menu\n")
        opcao = int(opcao)
    if opcao == 1:
        for proprietario in proprietarios:
            listar_proprietario(proprietario)
    elif(opcao == 2):
        posicao_proprietario = buscar_proprietario(proprietarios)
        if posicao_proprietario != -1:
            print("Um proprietario encontrado.\n")
            listar_proprietario(proprietarios[posicao_proprietario])
        else:
            print("Nenhum proprietario encontrado.\n")
    else:
        pass

def listar_proprietario(proprietario):
    print("CPF: {}\nNome: {}\n".format(*proprietario))


def get_cpf():
    cpf = ""
    while(len(cpf) != 11):
        cpf = input("Digite o cpf do proprietario (apenas números):\n")
    return cpf

def get_chassi():
    chassi = ""
    while(len(chassi) != 17):
        chassi = input("Digite o chassi do carro (apenas números):\n")
    return chassi

def get_placa():
    placa = ""
    while(len(placa) != 7):
        placa = input("Digite o placa do carro:\n")
    return placa

def get_nome():
    nome = ""
    while(len(nome) < 2):
        nome = input("Digite o nome do proprietario:")
    return nome

def get_modelo():
    modelo = ""
    while(len(modelo) < 2):
        modelo = input("Digite o modelo do carro:")
    return modelo

def get_marca():
    marca = ""
    while(len(marca) < 2):
        marca = input("Digite o marca do carro:")
    return marca

def editar_proprietario(proprietarios):
    posicao_proprietario = buscar_proprietario(proprietarios)
    if posicao_proprietario != -1:
        print("Um proprietario encontrado.\n")
        listar_proprietario(proprietarios[posicao_proprietario])
        opcao = input("Deseja editar proprietario? (S/n)\n")
        if(opcao in ["S", "s"]):
            proprietario = proprietarios[posicao_proprietario]
            opcao = input("Deseja editar o cpf? (S/n)\n")
            if(opcao in ["S", "s"]):
                proprietario[cpf_posicao] = get_cpf()
            else:
                pass
            opcao = input("Deseja editar o nome? (S/n)\n")
            if(opcao in ["S", "s"]):
                proprietario[nome_posicao] = get_nome()
            else:
                pass

            proprietarios[posicao_proprietario] = proprietario
            proprietarios.sort(key=sortProprietarioByCPF)
            save_proprietarios(proprietarios)
        else:
            pass

    else:
        print("Nenhum proprietario encontrado.\n")
    return proprietarios

def excluir_proprietario(proprietarios):
    posicao_proprietario = buscar_proprietario(proprietarios)
    if posicao_proprietario != -1:
        print("Um proprietario encontrado.\n")
        listar_proprietario(proprietarios[posicao_proprietario])
        opcao = input("Deseja excluir proprietario? (S/n)\n")
        if(opcao in ["S", "s"]):
            proprietario = proprietarios[posicao_proprietario]
            opcao = input("Tem certeza disso? (S/n)\n")
            if(opcao in ["S", "s"]):
                proprietarios.remove(proprietario)
            else:
                pass
            proprietarios.sort(key=sortProprietarioByCPF)
            save_proprietarios(proprietarios)
        else:
            pass

    else:
        print("Nenhum proprietario encontrado.\n")
    return proprietarios

def cadastrar_carro(carros, proprietarios):
    cpf = get_cpf()
    chassi = get_chassi()
    placa = get_placa()
    marca = get_marca()
    modelo = get_modelo()
    if(any(cpf in proprietario[cpf_posicao] for proprietario in proprietarios)):
        if(not any(chassi in carro[chassi_posicao] for carro in carros)):
            if(not any(placa in carro[placa_posicao] for carro in carros)):
                novo_carro = [cpf, chassi, placa, marca, modelo]
                carros.append(novo_carro)
                carros.sort(key=sortCarroByCPF)
            else:
                print("carro já cadastrado")
        else:
            print("carro já cadastrado")
    else:
        print("Proprietario não existe")

    save_carros(carros)
    return carros

def editar_carro(carros, proprietarios):
    posicao_carro = buscar_carro_por_placa(carros)
    if posicao_carro != -1:
        print("Um carro encontrado.\n")
        listar_carro(carros[posicao_carro])
        opcao = input("Deseja editar carro? (S/n)\n")
        if(opcao in ["S", "s"]):
            carro = carros[posicao_carro]
            opcao = input("Deseja editar o proprietario? (S/n)\n")
            if(opcao in ["S", "s"]):
                cpf = get_cpf()
                if(any(cpf in proprietario[cpf_posicao] for proprietario in proprietarios)):
                    carro[cpf_proprietario_posicao] = cpf
                else:
                    print("Proprietario não existe")
            else:
                pass
            opcao = input("Deseja editar o chassi? (S/n)\n")
            if(opcao in ["S", "s"]):
                chassi = get_chassi()
                if(len([chassi in carro[chassi_posicao] for carro in carros]) == 1):
                    carro[chassi_posicao] = chassi
                else:
                    print("carro já cadastrado")
            else:
                pass
            opcao = input("Deseja editar o placa? (S/n)\n")
            if(opcao in ["S", "s"]):
                placa = get_placa()
                if(len([placa in carro[placa_posicao] for carro in carros]) == 1):
                    carro[placa_posicao] = placa
                else:
                    print("Carro já cadastrado.")
            else:
                pass
            opcao = input("Deseja editar o marca? (S/n)\n")
            if(opcao in ["S", "s"]):
                carro[marca_posicao] = get_marca()
            else:
                pass
            opcao = input("Deseja editar o modelo? (S/n)\n")
            if(opcao in ["S", "s"]):
                carro[modelo_posicao] = get_modelo()
            else:
                pass

            carros[posicao_carro] = carro
            carros.sort(key=sortCarroByCPF)
            save_carros(carros)
        else:
            pass

    else:
        print("Nenhum carro encontrado.\n")
    return carros

def excluir_carro(carros):
    posicao_carro = buscar_carro_por_placa(carros)
    if posicao_carro != -1:
        print("Um carro encontrado.\n")
        listar_carro(carros[posicao_carro])
        opcao = input("Deseja excluir carro? (S/n)\n")
        if(opcao in ["S", "s"]):
            carro = carros[posicao_carro]
            opcao = input("Tem certeza disso? (S/n)\n")
            if(opcao in ["S", "s"]):
                carros.remove(carro)
            else:
                pass
            carros.sort(key=sortCarroByCPF)
            save_carros(carros)
        else:
            pass

    else:
        print("Nenhum carro encontrado.\n")
    return carros

def buscar_carro_por_proprietario(carros):
    posicao_carro = -1
    cpf = get_cpf()
    if(any(cpf in carro[cpf_proprietario_posicao] for carro in carros)):
        for i in range(len(carros)):
            if cpf == carros[i][cpf_posicao]:
                posicao_carro = i
                break
    else:
        pass
    return posicao_carro

def buscar_carro_por_chassi(carros):
    posicao_carro = -1
    chassi = get_chassi()
    if(any(chassi in carro[chassi_posicao] for carro in carros)):
        for i in range(len(carros)):
            if chassi == carros[i][chassi_posicao]:
                posicao_carro = i
                break
    else:
        pass
    return posicao_carro

def buscar_carro_por_placa(carros):
    posicao_carro = -1
    placa = get_placa()
    if(any(placa in carro[placa_posicao] for carro in carros)):
        for i in range(len(carros)):
            if placa == carros[i][placa_posicao]:
                posicao_carro = i
                break
    else:
        pass
    return posicao_carro

def listar_carros(carros):
    opcao = -1
    while(opcao not in [0, 1, 2, 3, 4]):
        opcao = input("\n1-Listar todos\n2-Buscar por Proprietario\n3-Buscar por Chassi\n4-Buscar por placa\n0-Voltar ao menu\n")
        opcao = int(opcao)
    if opcao == 1:
        for carro in carros:
            listar_carro(carro)
    elif(opcao == 2):
        posicao_carro = buscar_carro_por_proprietario(carros)
        if posicao_carro != -1:
            print("Um carro encontrado.\n")
            listar_carro(carros[posicao_carro])
        else:
            print("Nenhum carro encontrado.\n")
    elif(opcao == 3):
        posicao_carro = buscar_carro_por_chassi(carros)
        if posicao_carro != -1:
            print("Um carro encontrado.\n")
            listar_carro(carros[posicao_carro])
        else:
            print("Nenhum carro encontrado.\n")
    elif(opcao == 4):
        posicao_carro = buscar_carro_por_placa(carros)
        if posicao_carro != -1:
            print("Um carro encontrado.\n")
            listar_carro(carros[posicao_carro])
        else:
            print("Nenhum carro encontrado.\n")
    else:
        pass

def listar_carro(carro):
        print("Proprietario: {}\nChassi: {}\nPlaca: {}\nMarca: {}\nModelo: {}\n".format(*carro))

def save_carros(carros):
    carros.sort(key=sortCarroByCPF)
    with open(arquivo_carros, 'w', newline='') as myfile:

        # Adiciona a linha do cabeçalho
        carros = [["cpf_proprietario", "chassi", "placa", "marca", "modelo"]] + carros
        for carro in carros:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
            wr.writerow(carro)

def load_carros():
    with open(arquivo_carros, 'r') as f:
        reader = csv.reader(f)
        csv_list = list(reader)

        # Pula a linha do cabeçalho
        carros = csv_list[1:]
        carros.sort(key=sortCarroByCPF)
    return carros

def sortCarroByCPF(carro):
    return carro[cpf_proprietario_posicao]

def save_proprietarios(proprietarios):
    proprietarios.sort(key=sortProprietarioByCPF)
    with open(arquivo_proprietarios, 'w', newline='') as myfile:

        # Adiciona a linha do cabeçalho
        proprietarios = [["cpf", "nome"]] + proprietarios
        for proprietario in proprietarios:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
            wr.writerow(proprietario)

def load_proprietarios():
    with open(arquivo_proprietarios, 'r') as f:
        reader = csv.reader(f)
        csv_list = list(reader)

        # Pula a linha do cabeçalho
        proprietarios = csv_list[1:]
        proprietarios.sort(key=sortProprietarioByCPF)
    return proprietarios

def sortProprietarioByCPF(proprietario):
    return proprietario[cpf_posicao]

if __name__ == '__main__':
    main()
